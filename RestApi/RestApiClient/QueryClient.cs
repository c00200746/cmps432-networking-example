﻿using RestSharp;
using System.Collections.Generic;

namespace RestApiClient {
    public class QueryClient {
        private readonly RestClient _client;
        public QueryClient(string server) {
            _client = new RestClient(server);
        }

        public string[] Query(string query) {
            var request = new RestRequest("query");
            request.AddQueryParameter("query", query);
            return _client.Get<List<string>>(request).Data.ToArray();
        }
    }
}
