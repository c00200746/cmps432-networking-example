﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SharedLibrary;

namespace RestApiServer.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class QueryController : ControllerBase {
        private readonly ILogger<QueryController> _logger;
        private readonly QueryEngine _queryEngine;

        public QueryController(ILogger<QueryController> logger, QueryEngine queryEngine) {
            _logger = logger;
            _queryEngine = queryEngine;
        }

        [HttpGet]
        public string[] Get(string query) {
            if (!string.IsNullOrEmpty(query)) {
                var terms = query.Split(',').Select(s => s.Trim().ToLower()).ToArray();
                return _queryEngine.Query(terms);
            }
            return Array.Empty<string>();
        }
    }
}
