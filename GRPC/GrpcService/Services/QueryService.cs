using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using SharedLibrary;
using Query;

namespace GrpcService {
    public class QueryService : Querier.QuerierBase {
        private readonly ILogger<QueryService> _logger;
        private readonly QueryEngine _queryEngine;

        public QueryService(ILogger<QueryService> logger, QueryEngine queryEngine) {
            _logger = logger;
            _queryEngine = queryEngine;
        }

        public override Task<QueryResponse> Query(QueryRequest request, ServerCallContext context) {
            var result = _queryEngine.Query(request.Terms.ToArray());
            var response = new QueryResponse();
            response.Results.AddRange(result);
            return Task.FromResult(response);
        }

        public override async Task StreamQuery(QueryRequest request, IServerStreamWriter<SingleQueryReponse> serverStreamWriter, ServerCallContext context) {
            var results = _queryEngine.Query(request.Terms.ToArray());
            foreach (var result in results) {
                if (context.CancellationToken.IsCancellationRequested) break;
                var response = new SingleQueryReponse{ Result = result };
                await serverStreamWriter.WriteAsync(response);
                await Task.Delay(5000);
            }
        }
    }
}