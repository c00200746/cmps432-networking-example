﻿using Grpc.Net.Client;
using Query;
using System.Linq;
using System.Threading;
using Grpc.Core;

namespace GrpcClient {
    public class QueryClient {
        private readonly Querier.QuerierClient _querierClient;
        public QueryClient(string grpcServer) {
            var channel = GrpcChannel.ForAddress(grpcServer);
            _querierClient = new Querier.QuerierClient(channel);
        }
        public string[] Query(string[] terms) {
            var request = new QueryRequest();
            request.Terms.AddRange(terms);
            var response = _querierClient.Query(request);
            return response.Results.ToArray();
        }

        public AsyncServerStreamingCall<SingleQueryReponse> StreamQuery(string[] terms, CancellationToken ct = default) {
            var request = new QueryRequest();
            request.Terms.AddRange(terms);
            return _querierClient.StreamQuery(request, cancellationToken: ct);
        }
    }
}
