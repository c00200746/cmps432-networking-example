﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;

namespace GrpcClient {
    class Program {
        static void Main(string[] args) {
            var client = new QueryClient("https://localhost:5001");
            var terms = new[] {"cancel", "station info", "test", "pick", "guards"};
            Console.WriteLine("Stream Example");
            StreamingExample(terms, client).Wait();
            Console.WriteLine("Canceled Stream Example");
            CancellationStreamingExample(terms, client).Wait();
            Console.WriteLine("Interactive Unary Example");
            InputLoop(client);
        }

        static async Task StreamingExample(string[] terms, QueryClient client) {
            using var call = client.StreamQuery(terms);
            try {
                await foreach (var result in call.ResponseStream.ReadAllAsync()) {
                    Console.WriteLine(result);
                }
            } catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled) {
                Console.WriteLine("Stream cancelled.");
            }
        }

        static async Task CancellationStreamingExample(string[] terms, QueryClient client) {
            var cts = new CancellationTokenSource(10000);
            using var call = client.StreamQuery(terms, cts.Token);
            try {
                await foreach (var result in call.ResponseStream.ReadAllAsync()) {
                    Console.WriteLine(result);
                }
            } catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled) {
                Console.WriteLine("Stream cancelled.");
            }
        }

        static void InputLoop(QueryClient client) {
            Console.WriteLine("Query as comma delimited string or exit to exit: ");
            var input = Console.ReadLine()?.Trim();

            if (string.IsNullOrEmpty(input)) {
                Console.WriteLine("You have to give me something to work with here...");
                InputLoop(client);
            } else if (input.Equals("exit", StringComparison.OrdinalIgnoreCase)) {
                Console.WriteLine("Goodbye.");
                Environment.Exit(0);
            } else {
                //Get the query terms and normalize them (trim white space and make lower case)
                var queryTerms = input.Split(',').Select(s => s.Trim().ToLower()).ToArray();
                var results = client.Query(queryTerms);

                if (results.Any()) {
                    Console.WriteLine("Server sent the following results:");
                    foreach (var result in results)
                        Console.WriteLine(result);
                } else {
                    Console.WriteLine("Server returned no results for the query term(s).");
                }
                Console.WriteLine();
                InputLoop(client);
            }
        }
    }
}
