﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SharedLibrary {
    public class QueryEngine {
        private readonly string _indexPath;
        private Dictionary<string, string> _cache = new Dictionary<string, string>();

        public QueryEngine(string indexPath) {
            if (!File.Exists(indexPath)) 
                throw new ArgumentException($"{indexPath} is not a valid file", indexPath);
            
            _indexPath = indexPath;
        }

        public string[] Query(string[] terms) {
            return terms.Select(ResolveTerm).Where(x => x != null).ToArray();
        }

        /// <summary>
        /// Attempts to resolve terms with results. 
        /// </summary>
        /// <param name="term">The term to search for.</param>
        /// <returns>The result of the search. Can be null.</returns>
        private string ResolveTerm(string term) {
            //Check cache for term.
            if (_cache.TryGetValue(term, out var cacheResult))
                return cacheResult;
            
            //The value was not in cache, must get from file.
            var result = File.ReadAllLines(_indexPath).FirstOrDefault(line => line.StartsWith(term+"|."));
            //Add it to the cache.
            _cache.Add(term, result);
            return result;
        }
    }
}