using System.IO;

namespace SharedLibrary {
    public static class StringUtilities {
        
        public static byte[] GetBytes(string[] results) {
            using var ms = new MemoryStream();
            using var bw = new BinaryWriter(ms); 
            
            bw.Write((ushort)results.Length);
            foreach (var result in results)
                bw.Write(result);

            return ms.ToArray();
        }

        public static string[] GetStringArray(byte[] bytes) {
            using var ms = new MemoryStream(bytes);
            using var br = new BinaryReader(ms);

            var count = br.ReadUInt16();
            var strings = new string[count];
            for (var i = 0; i < count; i++)
                strings[i] = br.ReadString();
            return strings;
        }
    }
}