using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using SharedLibrary;

namespace SocketClient {
    public class QueryClient {
        private readonly IPEndPoint _remoteEndPoint;
        
        public QueryClient(string host, int port) {
            var ip = Dns.GetHostAddresses(host).First(a => a.AddressFamily == AddressFamily.InterNetwork);
            _remoteEndPoint = new IPEndPoint(ip, port);
        }

        public string[] Query(string[] queryTerms) {
            //Create the disposable client that is used to connect to the server.
            using var clientSocket = new Socket(_remoteEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            //Connect to the server.
            clientSocket.Connect(_remoteEndPoint);
            
            //Send the query terms to the server.
            var request = StringUtilities.GetBytes(queryTerms);
            clientSocket.Send(request);
            
            //Create a buffer to hold the response from the server. 
            var buffer = new byte[2048];
            
            //Receive the results from the server and trim the buffer. 
            var length = clientSocket.Receive(buffer);
            buffer = buffer.Take(length).ToArray();
            
            //Shutdown read and write streams and close the connection.
            clientSocket.Shutdown(SocketShutdown.Both);
            clientSocket.Close();
            return StringUtilities.GetStringArray(buffer);
        }
    }
}