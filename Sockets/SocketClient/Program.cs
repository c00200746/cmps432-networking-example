﻿using System;
using System.Linq;

namespace SocketClient {
    class Program {
        static void Main(string[] args) {
            var client = new QueryClient("localhost", 4200);
            InputLoop(client);
        }

        static void InputLoop(QueryClient client) {
            Console.WriteLine("Query as comma delimited string or exit to exit: ");
            var input = Console.ReadLine()?.Trim();

            if (string.IsNullOrEmpty(input)) {
                Console.WriteLine("You have to give me something to work with here...");
                InputLoop(client);
            }
            else if (input.Equals("exit", StringComparison.OrdinalIgnoreCase)) {
                Console.WriteLine("Goodbye.");
                Environment.Exit(0);
            }
            else {
                //Get the query terms and normalize them (trim white space and make lower case)
                var queryTerms = input.Split(',').Select(s => s.Trim().ToLower()).ToArray();
                var results = client.Query(queryTerms);
                
                if (results.Any()) {
                    Console.WriteLine("Server sent the following results:");
                    foreach (var result in results) 
                        Console.WriteLine(result);
                }
                else {
                    Console.WriteLine("Server returned no results for the query term(s).");
                }
                Console.WriteLine();
                InputLoop(client);
            }
        }
    }
}