using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using SharedLibrary;

namespace SocketServer {
    public class QueryServer {
        private readonly QueryEngine _queryEngine;
        private readonly Socket _socket;
        private readonly IPEndPoint _localEndPoint;

        public QueryServer(int port, QueryEngine queryEngine) {
            //Create a TCP socket
            _socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            _localEndPoint = new IPEndPoint(IPAddress.Any, port);
            _queryEngine = queryEngine;
        }

        public void Start() {
            //Bind the socket to any IP and port. 
            _socket.Bind(_localEndPoint);
            _socket.Listen(50);
            Console.WriteLine($"Ready to accept connection at {_socket.LocalEndPoint}");
            SocketLoop();
        }

        private void SocketLoop() {
            using (var clientSocket = _socket.Accept()) {
                Console.WriteLine($"New client connected from {clientSocket.RemoteEndPoint}");
                var buffer = new byte[2048];
                
                //Receive the query request and put it in the buffer.
                var length = clientSocket.Receive(buffer);
                //Trim the buffer to the size of the request to remove excess.
                buffer = buffer.Take(length).ToArray();
                //Handle the request and get a result
                var result = HandleQueryRequest(buffer);
                //Send the result to the client
                clientSocket.Send(result);
            }
            //Falling out of the using disconnects the client, recurse to accept another client.
            SocketLoop();
        }

        private byte[] HandleQueryRequest(byte[] requestData) {
            var terms = StringUtilities.GetStringArray(requestData);
            var results = _queryEngine.Query(terms);
            return StringUtilities.GetBytes(results);
        }
    }
}