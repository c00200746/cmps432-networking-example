﻿using System;
using System.IO;
using System.Text;
using SharedLibrary;

namespace SocketServer {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Starting socket server... You must close the application to stop the server.");
            var engine = new QueryEngine(Constants.IndexPath);
//            var s1 = engine.Query(new[] {"test", "gold"});
//            var s2 = engine.Query(new[] {"test", "pico", "asssssd"});
//            var s3 = engine.Query(new[] {"asssssd"});
//
//            var bytes1 = GetBytes(s1);
//            var bytes2 = GetBytes(s2);
//            var bytes3 = GetBytes(s3);
//            
//            var r1 = GetStringArray(bytes1);
//            var r2 = GetStringArray(bytes2);
//            var r3 = GetStringArray(bytes3);

            
            var server = new QueryServer(4200, engine);
            server.Start();
        }
        
    }
}